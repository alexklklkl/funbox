function getClassName(str) {
    return str.split('_')[0];
}

let DOM = {
    createItemElement: function(itemData) {
        let itemTemplate = document.getElementById("item_template");
        let templateElement = itemTemplate.content.cloneNode(true);
        let itemElement = templateElement.querySelector('.item');

        for (let key in itemData) {
            let node = itemElement.querySelector('.' + key);
            if (node) {
                // есть узел с именем класса, совпадающим с именем свойства
                node.innerHTML = itemData[key];
            } else {
                switch (key) {
                    case 'image':
                        itemElement.querySelector('img.inner-image').src = "images/" + itemData[key] + ".png";
                        break;
                    // можно brand_s_h, comments_s и т.д. поместить в массив
                    // и проверять через includes
                    case 'brand_s_h': // текст для brand selected hover
                    case 'comments_s': // текст для comments selected
                        // для элемента установим атрибут data-storage
                        let node = itemElement.querySelector('.' + getClassName(key));
                        if (node) {
                            node.dataset['storage'] = itemData[key];
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        /*
        * Установим enabled, selected для элемента
        */
        if (itemData.enabled) {
            itemElement.classList.add('default');
            itemElement.classList.add('hover-enabled');
            if (itemData.selected) {
                setSelected(itemElement);
            }
        } else {
            itemElement.classList.add('disabled');
        }
        return itemElement;
    }
}