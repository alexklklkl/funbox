/*
* Тестовые данные
* Суффиксы:
* _s - selected
* _h - hover
* _d - disabled
*/
let data = [
    {
        image: 'dog_2',
        brand: 'Сказочное русское кушанье',
        brand_s_h: 'Волчёнку не нравится?',
        title: 'Вкусняшка',
        sub_title: 'для юных',
        description: '<b>90</b> порций<br />Коты в подарок не положены',
        value: '4',
        comments: 'Порадуй маленького, <a href="#">купи</a>',
        comments_s: 'Витамины и минералы для роста',
        enabled: true,
        selected: false,
    },
    {
        image: 'dog_1',
        brand: 'Сказочное русское кушанье',
        brand_s_h: 'Волку не нравится?',
        title: 'Вкусняшка',
        sub_title: 'для активных',
        description: '<b>90</b> порций<br /><b>2</b> кота в подарок<br />котов вернуть в магазин',
        value: '4,5',
        comments: 'Порадуй друга, <a href="#">купи</a>',
        comments_s: 'Баланс для активной жизни',
        enabled: true,
        selected: true,
    },
    {
        image: 'dog_3',
        brand: 'Сказочное русское кушанье',
        brand_s_h: 'Матёрому не нравится?',
        title: 'Вкусняшка',
        sub_title: 'для опытных',
        description: '<b>90</b> порций<br />Коты уже не интересуют',
        value: '5',
        comments: 'Порадуй бывалого друга, <a href="#">купи</a>',
        comments_s: 'Вкусно и полезно',
        enabled: true,
        selected: false,
    },
    {
        image: 'cat',
        brand: 'Сказочное заморское яство',
        brand_s_h: '',
        title: 'Нямушка',
        sub_title: 'для жирных котов',
        description: '<b>90</b> порций<br />2 мыши в подарок',
        value: '5',
        comments: 'Коты переехали за угол',
        comments_s: '',
        enabled: false,
        selected: false,
    },
];

/*
* Возвращаем json данные в get()
*/
let Data = {
    get: function() {
        /*
        * TODO - сделать запрос к серверу, например через fetch()
        * А пока - массив объектов
        * Вернем json с "откуда-то полученными" данными
        */
        return JSON.stringify(data);
    },
};
