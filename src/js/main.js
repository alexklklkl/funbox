/*
* Точка входа
* Основная логика по загрузке, парсингу, добавлению контента и регистрации listener'ов событий
*/
document.addEventListener("DOMContentLoaded", () => {
    init();
});

/*
* Цепочка функций для инициализации контента
* (в ie, как и весь es6, работает через babel/polyfills)
*/
function init() {
    Promise.resolve()
        .then(requestData)
        .then(verifyData)
        .then(parseData)
        .then(createDOMObjects)
        .then(addObjectsToDOM)
        .then(setListeners)
        .catch(error);
}

/*
* Получим данные, ожидаем json
*/
function requestData() {
    let data = Data.get();
    return data;
}

/*
* TODO - для тестового задания считаем, что данные достоверны
* В реальной жизни надо бы проверить...
*/
function verifyData(data) {
    return data;
}

function parseData(data) {
    return JSON.parse(data);
}

function createDOMObjects(itemsData) {
    let itemsElement = itemsData.map((itemData) => {
        return DOM.createItemElement(itemData);
    });
    return itemsElement;
}

/*
* Обернем элементы (для корректной иерархии), добавим в DOM.
* Вернем массив добавленных элементов
* (для последующего навешивания listener'ов)
*/
function addObjectsToDOM(itemElements) {
    let domElements = itemElements.map((itemElement) => {
        // Обернем itemElement в <div class='col' /> (для верстки)
        let wrapper = document.createElement('div');
        wrapper.classList.add('col');
        wrapper.appendChild(itemElement);
        // itemsContainer - id блока-контейнера в DOM для добавления элементов
        let domElement = itemsContainer.appendChild(wrapper);
        // Вернуть не весь добавленный элемент, а .item
        return domElement.querySelector(".item");
    });
    return domElements;
}


function setListeners(items) {
    items.forEach((item) => {
        if (!item.classList.contains('disabled')) {
            // элементы для событий - карточка и ссылка в описании
            let topContainer = item.querySelector('.top-container');

            // click на карточке
            topContainer.addEventListener('click', () => {
                Events.click(item);
            });
            // click на ссылке в описании (если есть)
            let commentsA = item.querySelector('.comments a');
            if (commentsA) {
                commentsA.addEventListener('click', (event) => {
                    event.preventDefault();
                    Events.click(item);
                });
            }

            item.addEventListener('mouseenter', () => {
                Events.mouseEnter(item);
            });

            item.addEventListener('mouseleave', () => {
                Events.mouseLeave(item);
            });
        }
    });
    return true;
}

function error(err) {
    console.log(err);
}

