/*
* добавим в Element функции для вставки контента из атрибута data-storage
* с возможностью восстановления предыдущего значения из data-storage_
*/
Element.prototype.setHtmlFromData = function() {
    if (this.dataset['storage']) {
        this.dataset['storage_'] = this.innerHTML;
        this.innerHTML = this.dataset['storage'];
    }
}
Element.prototype.restoreHtmlFromData = function() {
    if (this.dataset['storage_']) {
        this.innerHTML = this.dataset['storage_'];
    }
}

// Функции для упрощения работы с классами узлов DOM
Element.prototype.hasClass = function(className) {
    return this.classList.contains(className);
}
Element.prototype.addClass = function(className) {
    if (!this.hasClass(className)) {
        this.classList.add(className);
    }
}
Element.prototype.removeClass = function(className) {
    if (this.hasClass(className)) {
        this.classList.remove(className);
    }
}

// Функции для выделения и снятия выделения элемента
let setSelected = function(item) {
    if (item.hasClass('item')) {
        item.addClass('selected');
        // Поменяем контент в описании
        let comments = item.querySelector('.comments')
        comments.setHtmlFromData();
    }
}
let unsetSelected = function(item) {
    if (item.hasClass('item')) {
        item.removeClass('selected');
        let brand = item.querySelector('.brand');
        brand.restoreHtmlFromData();
        // Восстановим оригинальный контент
        let comments = item.querySelector('.comments')
        comments.restoreHtmlFromData();
    }
}

let Events = {
    click: function(item) {
        if (!item.hasClass('selected')) {
            setSelected(item);
        } else {
            unsetSelected(item);
            // И снова зарегистрируем слушателя для элемента (.comments a)
            // Другой вариант - использование "живого" слушателя,
            // зарегистрированного для всего document,
            // и фильтровать элементы по e.target.
            let commentsA = item.querySelector('.comments a');
            if (commentsA) {
                commentsA.addEventListener('click', function(event) {
                    event.preventDefault();
                    this.click(item);
                }.bind(this));
            }
        }
        // Удалив класс .hover-enabled, запретим :hover после клика. Снова разрешим в mouseLeave
        item.removeClass('hover-enabled');
    },

    /*
    * Заменим контент в .brand для выделенного элемента
    * Для замены и восстановления контента в узле .brand можно не использовать js (обойтись css),
    * создав два узла с разным содержимым (а для comments - три узла) и настроив display:none/block в :hover
    * Может быть так и лучше, но не хочется "засорять" DOM
    */
    mouseEnter: function(item) {
        if (
            item.hasClass('selected')
            && item.hasClass('hover-enabled')
        ) {
            let brand = item.querySelector('.brand');
            brand.setHtmlFromData();
        }
    },

    mouseLeave: function(item) {
        // Разрешим :hover
        if (!item.hasClass('hover-enabled')) {
            item.addClass('hover-enabled');
        } else {
            // Восстановим контент в .brand для выделенного элемента
            if (
                item.hasClass('selected')
            ) {
                let brand = item.querySelector('.brand');
                brand.restoreHtmlFromData();
            }
        }
    }
}
