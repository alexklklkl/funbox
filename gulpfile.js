'use strict';
const SRC_DIR = 'src/';
const BUILD_DIR = 'build/';

const { src, dest, series, parallel, watch } = require('gulp');
const gulpif = require('gulp-if');
const terser = require('gulp-terser');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const webp = require('gulp-webp');
const log = require('fancy-log');
const del = require("del");
const babel = require("gulp-babel");
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();

/*
* Задачи для каждого типа контента
* */
let tasks = {
    html: {
        src: SRC_DIR + '*.html',
        build: BUILD_DIR,
        task: html
    },
    js: {
        src: SRC_DIR + 'js/**/*.js',
        // Для конкатенации важна последовательность
        srcList: [
            'polyfills/es6',
            'polyfills/template',
            'data',
            'events',
            'dom',
            'main',
        ],
        build: BUILD_DIR + 'js/',
        task: js
    },
    styles: {
        src: SRC_DIR + 'styles/**/*.scss',
        build: BUILD_DIR + 'styles/',
        task: styles
    },
    images: {
        src: SRC_DIR + 'images/**/*.*',
        build: BUILD_DIR + 'images/',
        task: images
    },
    // отделим картинки дизайна от основных в images
    images_styles: {
        src: SRC_DIR + 'styles/images/**/*.*',
        build: BUILD_DIR + 'styles/images/',
        task: images
    },
    fonts: {
        src: SRC_DIR + 'styles/fonts/**/*.*',
        build: BUILD_DIR + 'styles/fonts/',
        task: fonts
    },
};

// ---------------- options ------------------
let saasOptions = {
    includePaths: require('node-normalize-scss').includePaths,
    //outputStyle: 'compressed'
};

// ---------------- tasks ------------------
/*
* Задачи для каждого tasks
* */
function html() {
    // this будет объектом taskObject
    return () => {
        //  вернем Promise
        return src(this.src)
            .pipe(dest(this.build))
            .pipe(gulpif(!isProduction(), browserSync.reload({stream:true})));
    }
}
function js() {
    return () => {
            // Список файлов в нужной последовательности
            return src(this.srcList.map((file) => {
                return `${SRC_DIR}js/${file}.js`;
            }))
            // TODO - не обрабатывать polyfills в babel
            .pipe(babel())
            .pipe(concat('main.js'))
            .pipe(gulpif(isProduction(), terser()))
            .pipe(dest(this.build))
            .pipe(gulpif(!isProduction(), browserSync.reload({stream:true})));
    }
}
function styles() {
    return () => {
        return src(this.src)
            .pipe(sass(saasOptions))
            .pipe(sass().on('error', sass.logError))
            .pipe(gulpif(isProduction(), cssnano()))
            .pipe(dest(this.build))
            .pipe(gulpif(!isProduction(), browserSync.stream()));
    }
}
function fonts() {
    return () => {
        return src(this.src)
            .pipe(dest(this.build));
    }
}
function images() {
    return () => {
        return src(this.src)
            //.pipe(webp()) - safari и ie не поддерживают...
            .pipe(dest(this.build))
            .pipe(gulpif(!isProduction(), browserSync.reload({stream:true})));
    }
}
function images_styles() {
    return () => {
        return src(this.src)
            //.pipe(webp()) - safari и ie не поддерживают...
            .pipe(dest(this.build))
            .pipe(gulpif(!isProduction(), browserSync.reload({stream:true})));
    }
}

// ---------------- Сборка задач ------------------
let queue =  [];
for (let key in tasks) { // по всем типам контента
    if (typeof tasks[key] !== 'undefined' && typeof tasks[key].task === 'function') {
        // Для сокращения дальнейшей записи
        // taskObject - объект для каждого типа контента
        let taskObject = tasks[key];

        // соберем задачи
        queue.push(taskObject.task.call(taskObject));

        // отдельный watch на каждый task
        watch(taskObject.src, /*{ignoreInitial: false},*/ taskObject.task.call(taskObject));
    }
}

// ---------------- server and helpers ------------------
function clean() {
    return del(BUILD_DIR);
}
function serve(done) {
    browserSync.init({
        server: {
            baseDir: BUILD_DIR,
        }
    });
    if (done)
        done();
}
function isProduction() {
    return process.env.NODE_ENV == 'production';
}

// ---------------- export ------------------

exports.default = series(clean, parallel(queue), serve);

